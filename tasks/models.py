from django.db import models
from django.contrib.auth.models import User


# Create your models here.


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        "projects.Project", on_delete=models.CASCADE, related_name="tasks"
    )
    assignee = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="tasks", null=True
    )
