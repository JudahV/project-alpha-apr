
from . import views
from django.urls import path

from .views import logout_view, user_signup

urlpatterns = [
    path("login/", views.login_view, name="login"),
    path("logout/", logout_view, name="logout"),
    path("signup/", user_signup, name="signup"),
]
