from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, related_name="projects"
    )
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="projects", null=True
    )

    def __str__(self):
        return self.name
